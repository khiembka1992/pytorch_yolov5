import argparse
import time
from pathlib import Path

import numpy as np
import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
from shapely.geometry import box, Polygon

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, increment_path
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized

def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype = "float32")
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect

def four_point_transform(image, pts, pad:int = 0):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    # maxWidth = max(int(widthA), int(widthB))
    maxWidth = 900
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    # maxHeight = max(int(heightA), int(heightB))
    maxHeight = 600
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [pad, pad],
        [maxWidth + pad - 1, pad],
        [maxWidth + pad - 1, maxHeight + pad - 1],
        [pad, maxHeight + pad - 1]], dtype = "float32")
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth + 2*pad, maxHeight + 2*pad))
    # return the warped image
    return warped

# from shapely.geometry import box, Polygon

def iou_poly(pol1_xy, pol2_xy):
    # print(pol1_xy)
    # print(pol2_xy)
    # Define Each polygon 
    # pol1_xy = [[130, 27], [129.52, 27], [129.45, 27.1], [130.13, 26]]
    # pol2_xy = [[30, 27.200001], [129.52, 27.34], [129.45, 27.1], [130.13, 26.950001]]
    polygon1_shape = Polygon(pol1_xy)
    polygon2_shape = Polygon(pol2_xy)

    # Calculate Intersection and union, and tne IOU
    polygon_intersection = polygon1_shape.intersection(polygon2_shape).area
    polygon_union = polygon1_shape.union(polygon2_shape).area
    IOU = polygon_intersection / polygon_union
    return IOU

def detect(save_img=False):
    source, weights, view_img, save_txt, imgsz = opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size
    save_img = not opt.nosave and not source.endswith('.txt')  # save inference images
    webcam = source.isnumeric() or source.endswith('.txt') or source.lower().startswith(
        ('rtsp://', 'rtmp://', 'http://', 'https://'))

    # Directories
    save_dir = Path(increment_path(Path(opt.project) / opt.name, exist_ok=opt.exist_ok))  # increment run
    (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir
    (save_dir / 'losscorner').mkdir(parents=True, exist_ok=True)
    (save_dir / 'align').mkdir(parents=True, exist_ok=True)
    (save_dir / 'nocard').mkdir(parents=True, exist_ok=True)

    # Initialize
    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(imgsz, s=stride)  # check img_size
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model']).to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = check_imshow()
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz, stride=stride)
    else:
        dataset = LoadImages(source, img_size=imgsz, stride=stride)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in names]

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once
    t0 = time.time()
    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
        det = pred[0]
        p, s, im0, frame = path, '', im0s, getattr(dataset, 'frame', 0)

        p = Path(p)  # to Path
        save_path = str(save_dir / p.name)  # img.jpg
        txt_path = str(save_dir / 'labels' / p.stem) + ('' if dataset.mode == 'image' else f'_{frame}')  # img.txt
        s += '%gx%g ' % img.shape[2:]  # print string
        gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh

        if len(det) == 0:
            cv2.imwrite(str(save_dir / "nocard" /p.name), im0)
            continue

        # Rescale boxes from img_size to im0 size
        det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

        # Print results
        for c in det[:, -1].unique():
            n = (det[:, -1] == c).sum()  # detections per class
            s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

        # Write results
        #['cccd_0', 'cccd_1', 'chip_id_0', 'chip_id_1', 'new_id_0', 'new_id_1', 'old_id_0', 'old_id_1', 'passport']
        # wh_cm = [(85.6,53.98), (85.6,53.98), (85.6,53.98), (85.6,53.98), (85.6,53.98), (85.6,53.98), (85.6,53.98), (85.6,53.98), (15.5,10.5)] 
        
        max_box_card = None
        # point_corners = [] #[xy]
        for *xyxy, conf, cls in reversed(det):
            xyxy = [pt.item() for pt in xyxy]
            conf = conf.item()
            cls = cls.item()

            if cls != 9:
                if max_box_card is None or max_box_card[1] < conf:
                    max_box_card = [tuple(xyxy),conf,cls, []]

        if max_box_card is None:
            cv2.imwrite(str(save_dir / "nocard" /p.name), im0)
            continue

        img0crop = im0[int(max_box_card[0][1]):int(max_box_card[0][3]),int(max_box_card[0][0]):int(max_box_card[0][2])].copy()  # BGR
        
        width_pixel = abs(max_box_card[0][0] - max_box_card[0][2])
        height_pixel = abs(max_box_card[0][1] - max_box_card[0][3])
        if height_pixel > width_pixel:
            img0crop = np.rot90(img0crop)
        img0crop = cv2.copyMakeBorder(img0crop, int(height_pixel/10), int(height_pixel/10), int(width_pixel/10), int(width_pixel/10), cv2.BORDER_CONSTANT, 0)

        # cv2.imshow("crop", img0crop)
        # cv2.waitKey(0)
        # Padded resize

        from utils.datasets import letterbox

        imgcrop = letterbox(img0crop, dataset.img_size, stride=dataset.stride)[0]

        # Convert
        imgcrop = imgcrop[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        imgcrop = np.ascontiguousarray(imgcrop)
        imgcrop = torch.from_numpy(imgcrop).to(device)
        imgcrop = imgcrop.half() if half else imgcrop.float()  # uint8 to fp16/32
        imgcrop /= 255.0  # 0 - 255 to 0.0 - 1.0
        if imgcrop.ndimension() == 3:
            imgcrop = imgcrop.unsqueeze(0)

        predcrop = model(imgcrop, augment=opt.augment)[0]

        # Apply NMS
        predcrop = non_max_suppression(predcrop, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)

        # print(predcrop)

        detcrop = predcrop[0]

        if len(detcrop)==0:
            cv2.imwrite(str(save_dir / "nocard" /p.name), im0)
            continue
        # Rescale boxes from img_size to im0 size

        # print(detcrop[:, :4])
        detcrop[:, :4] = scale_coords(imgcrop.shape[2:], detcrop[:, :4], img0crop.shape).round()

        # Print results
        # for c in detcrop[:, -1].unique():
            # n = (detcrop[:, -1] == c).sum()  # detections per class
            # s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string
        point_corners = [] #[xy]
        max_box_card = None
        for *xyxy, conf, cls in reversed(detcrop):
            # print(cls)
            xyxy = [pt.item() for pt in xyxy]
            conf = conf.item()
            cls = cls.item()
            # width_pixel = xyxy[2] - xyxy[0]
            # height_pixel = xyxy[3] - xyxy[1]
            # wh_ratio = width_pixel/height_pixel

            if cls == 9:
                cx = (xyxy[2] + xyxy[0])/2
                cy = (xyxy[3] + xyxy[1])/2
                point_corners.append((cx,cy))
                continue

            #  if cls != 9:
            if max_box_card is None or max_box_card[1] < conf:
                max_box_card = [tuple(xyxy),conf,cls, []]

        if max_box_card is None:
            cv2.imwrite(str(save_dir / "nocard" /p.name), im0)
            continue
        
        xyxy, conf, cls, _ = max_box_card
        max_x = max(xyxy[0],xyxy[2])
        min_x = min(xyxy[0],xyxy[2])
        max_y = max(xyxy[1],xyxy[3])
        min_y = min(xyxy[1],xyxy[3])
        for point_corner in point_corners:
            if point_corner[0] > min_x and point_corner[0] < max_x and point_corner[1] > min_y and point_corner[1] < max_y:
                max_box_card[3].append(point_corner)

        final_point_corner = []

        # print(f"{len(max_box_card[3])} corner")
        
        if len(max_box_card[3]) == 3:
            pt_1st = np.array(max_box_card[3][0], np.float32)
            pt_2st = np.array(max_box_card[3][1], np.float32)
            pt_3st = np.array(max_box_card[3][2], np.float32)

            max_x = max(xyxy[0],xyxy[2])
            min_x = min(xyxy[0],xyxy[2])
            max_y = max(xyxy[1],xyxy[3])
            min_y = min(xyxy[1],xyxy[3])

            pol_base = [[xyxy[0],xyxy[1]],[xyxy[2],xyxy[1]],[xyxy[2],xyxy[3]],[xyxy[0],xyxy[3]]]
            
            # print(pt_1st, pt_2st, pt_3st)

            gen_4th_corner = None

            gen_4th_corner_tmp = (pt_2st + pt_3st) - pt_1st
            # print(gen_4th_corner_tmp)

            max_iou = 0
            iou = iou_poly([pt_1st.tolist(), pt_2st.tolist(), gen_4th_corner_tmp.tolist(), pt_3st.tolist()], pol_base)
            # print(iou)
            if iou > max_iou:
                max_iou = iou
                gen_4th_corner = gen_4th_corner_tmp

            gen_4th_corner_tmp = (pt_1st + pt_3st) - pt_2st
            # print(gen_4th_corner_tmp)

            iou = iou_poly([pt_1st.tolist(), pt_2st.tolist(), pt_3st.tolist(), gen_4th_corner_tmp.tolist()], pol_base)
            # print(iou)
            if iou > max_iou:
                max_iou = iou
                gen_4th_corner = gen_4th_corner_tmp

            gen_4th_corner_tmp = (pt_1st + pt_2st) - pt_3st
            # print(gen_4th_corner_tmp)

            iou = iou_poly([pt_1st.tolist(),gen_4th_corner_tmp.tolist(), pt_2st.tolist(), pt_3st.tolist()], pol_base)
            # print(iou)
            if iou > max_iou:
                iou = max_iou
                gen_4th_corner = gen_4th_corner_tmp

            # if gen_4th_corner is None:
                # cv2.imwrite(save_path.replace(".jpg", "notgen4th.jpg").replace(".png", "notgen4th.png"), img0crop)
                # continue
            if gen_4th_corner is not None:
                final_point_corner = max_box_card[3]
                final_point_corner.append(tuple(gen_4th_corner))
            
        elif len(max_box_card[3]) == 4:
            final_point_corner = max_box_card[3]

        elif len(max_box_card[3]) > 4:
            rect = cv2.minAreaRect(np.array(max_box_card[3], np.float32))
            box = cv2.boxPoints(rect)

            pt_corner_pt_box = {}   #pt_corner: pt_box

            for pt_box in box:
                min_dim = float('inf')
                min_pt_corner = None
                for pt_corner in max_box_card[3]:
                    if pt_corner in pt_corner_pt_box:
                        continue
                    dim = (pt_box[0]-pt_corner[0])**2 + (pt_box[1]-pt_corner[1])**2
                    if dim < min_dim:
                        min_dim = dim
                        min_pt_corner = pt_corner
                if min_pt_corner is None:
                    continue
                pt_corner_pt_box[min_pt_corner] = pt_box

            # print("pt_corner_pt_box", pt_corner_pt_box)
            final_point_corner = list(pt_corner_pt_box.keys())

        max_box_card[3] = final_point_corner

        if len(max_box_card[3]) < 4:
            dest_pts = [(max_box_card[0][0], max_box_card[0][1]),
                                    (max_box_card[0][0], max_box_card[0][3]),
                                    (max_box_card[0][2], max_box_card[0][1]),
                                    (max_box_card[0][2], max_box_card[0][3])]

            warped = four_point_transform(img0crop, np.array(dest_pts), 40)
            
            cv2.imwrite(str(save_dir / "losscorner" /p.name), warped)

        elif len(max_box_card[3]) == 4:
            warped = four_point_transform(img0crop, np.array(max_box_card[3]), 40)
            cv2.imwrite(str(save_dir / "align" /p.name), warped)

        if save_img or view_img:  # Add bbox to image
            label = f'{names[int(cls)]} {conf:.2f}'
            plot_one_box(xyxy, img0crop, label=label, color=colors[int(cls)], line_thickness=3)

        for pt_corner in max_box_card[3]:
            cv2.drawMarker(img0crop, (int(pt_corner[0]), int(pt_corner[1])), (0,255,0), cv2.MARKER_CROSS, 10, 3, 8)

        # Print time (inference + NMS)
        # print(f'{s}Done. ({t2 - t1:.3f}s)')

        # Stream results
        if view_img:
            cv2.imshow(str(p), img0crop)
            cv2.waitKey(1)  # 1 millisecond

        # Save results (image with detections)
        if save_img:
            cv2.imwrite(save_path, img0crop)

    if save_txt or save_img:
        s = f"\n{len(list(save_dir.glob('labels/*.txt')))} labels saved to {save_dir / 'labels'}" if save_txt else ''
        print(f"Results saved to {save_dir}{s}")

    print(f'Done. ({time.time() - t0:.3f}s)')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='yolov5s.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='data/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.25, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--save-conf', action='store_true', help='save confidences in --save-txt labels')
    parser.add_argument('--nosave', action='store_true', help='do not save images/videos')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--project', default='runs/detect', help='save results to project/name')
    parser.add_argument('--name', default='exp', help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    opt = parser.parse_args()
    print(opt)
    check_requirements(exclude=('pycocotools', 'thop'))

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt']:
                detect()
                strip_optimizer(opt.weights)
        else:
            detect()
